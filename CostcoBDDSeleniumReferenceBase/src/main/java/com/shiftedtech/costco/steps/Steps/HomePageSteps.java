package com.shiftedtech.costco.steps.Steps;

import com.shiftedtech.costco.steps.Base.StepBase;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

/**
 * Created by Mithu on 10/17/2017.
 */
public class HomePageSteps extends StepBase {

    @Given("^As a not validated user\\.$")
    public void as_a_not_validated_user() throws Throwable {
        driver.manage().deleteAllCookies();
    }

    @When("^Browse to the url \"([^\"]*)\"$")
    public void browse_to_the_url(String arg1) throws Throwable {
        driver.navigate().to(arg1);
    }

    @Then("^Costco home page should show$")
    public void costco_home_page_should_show() throws Throwable {

        WebElement element = driver.findElement(By.xpath("//title"));
        Thread.sleep(3000);
        String text = driver.getTitle();
        Assert.assertEquals("Welcome to Costco Wholesale",text);

    }
    @When("^User click login button$")
    public void user_click_login_button() throws Throwable {
        WebElement element = driver.findElement(By.xpath(".//*[@id='header_sign_in']"));
        element.click();
    }


}
