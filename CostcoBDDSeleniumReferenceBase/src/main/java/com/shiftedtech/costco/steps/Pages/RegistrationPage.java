package com.shiftedtech.costco.steps.Pages;

import com.shiftedtech.costco.steps.Base.StepBase;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

/**
 * Created by Mithu on 10/21/2017.
 */
public class RegistrationPage extends StepBase {

    WebDriver driver;
    public RegistrationPage(WebDriver driver){
        this.driver = driver;
    }

    @FindBy(xpath = ".//*[@id='header_sign_in']")
    public WebElement register;

    @FindBy(xpath = ".//*[@id='LogonForm']/fieldset/div[8]/a")
    public WebElement createAccount;

    @FindBy(xpath = ".//*[@id='register_email1']")
    public WebElement emailAddressTB;

    @FindBy(xpath = ".//*[@id='logonPassword']")
    public WebElement passWordTB;

    @FindBy(xpath = ".//*[@id='logonPasswordVerify']")
    public WebElement confirmPassWordTB;

    @FindBy(xpath = ".//*[@id='register_userField2']")
    public WebElement membershipNumberBox;

    @FindBy(xpath = ".//*[@id='RegisterForm']/fieldset/div[7]/input")
    public WebElement registerButton;


    public void accountCreation(String emailAddress, String passWord, String confirmPassWord, String membershipNumber){
        register.click();
        createAccount.click();
        emailAddressTB.sendKeys("emailAddress");
        // emailAddressTB.clear();
        passWordTB.sendKeys("passWord");
        confirmPassWordTB.sendKeys("confirmPassWord");
        membershipNumberBox.sendKeys("membershipNumber");
        registerButton.click();

    }
}
