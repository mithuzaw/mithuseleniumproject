package com.shiftedtech.costco.steps.Pages;

import com.shiftedtech.costco.steps.Base.StepBase;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;

/**
 * Created by Mithu on 10/21/2017.
 */
public class LoginPage extends StepBase {

    private WebDriver driver;
    public LoginPage(WebDriver driver) {
        this.driver = driver;
    }


    @FindBy(xpath = ".//*[@id='logonId']")
    private WebElement emailTextBox;

    @FindBy(xpath = ".//*[@id='logonPassword']")
    private WebElement passwordTextBox;

    @FindBy(xpath = ".//*[@id='LogonForm']/fieldset/div[5]/input")
    private WebElement logInButton;

    @FindBy(id = ".critical-notification.form-group")
    private WebElement errorMessage;



    public void enterEmailAddress(String email){
        emailTextBox.sendKeys(email);
    }

    public void enterPassword(String password){
        passwordTextBox.sendKeys(password);
    }

    public void clickLogin(){
        logInButton.click();
    }
    public void login(String user, String password){
        enterEmailAddress(user);
        enterPassword(password);
        clickLogin();
    }

    public void verifyErrorMessage(){
        String text = errorMessage.getText();
        Assert.assertEquals("The email address and/or password you entered are invalid.",text);

    }
}
