package com.shiftedtech.costco.steps.Pages;

import com.shiftedtech.costco.steps.Base.StepBase;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;

/**
 * Created by Mithu on 10/21/2017.
 */
public class HomePage extends StepBase{
    private WebDriver driver;

    public HomePage(WebDriver driver){
        this.driver = driver;
    }

    @FindBy(xpath = ".//*[@id='header_sign_in']")
    private WebElement login;

    @FindBy(id = "myaccount-d")
    private WebElement myAccount;



    public void gotoLoginPage() {
        login.click();
    }

    public void verifyHomePageDisplay(){
        WebElement element = driver.findElement(By.xpath("//title"));

        String text = element.getText();
        Assert.assertEquals("Welcome to Costco Wholesale",text);

    }

    public void verifyAccountName(String account){

        String text = myAccount.getText();
        Assert.assertEquals(account,text);
    }

}
