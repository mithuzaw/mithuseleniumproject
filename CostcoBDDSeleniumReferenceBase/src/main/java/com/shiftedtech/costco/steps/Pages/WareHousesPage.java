package com.shiftedtech.costco.steps.Pages;

import com.shiftedtech.costco.steps.Base.StepBase;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;

/**
 * Created by Mithu on 10/21/2017.
 */
public class WareHousesPage extends StepBase {

    public WareHousesPage(WebDriver driver) {
        this.driver = driver;
    }

    @FindBy(id = "warehouse-locations")
    protected WebElement wareHousesLocations;

    @FindBy(id = "warehouse-search-field")
    protected WebElement whSearchBox;

    @FindBy(xpath = "//input[@value='Find a Warehouse']")
    protected WebElement findWHButton;

    //To select the checkbox call gasStation from the list.

    public void selectMultipleCheckItems1() {
        List<WebElement> checkbox = driver.findElements(By.cssSelector(".style-check>label>span"));
        for (WebElement element : checkbox) {
            String name = element.getText();
            System.out.println("List of names: " + name);
            if (name.contentEquals("Gas Station")) {
                element.click();
                break;
            }

        }
    }

    @FindBy(xpath = "//a[@href='/warehouse-locations/rego-park-ny-1070.html']")
    protected WebElement storeDetailsLocations;


    @FindBy(id = "warehouse-locations")
    protected WebElement wareHousesLocationsLink;

    public void clickOnWarehouse(){
        mouseOn(wareHousesLocations);
        delayFor(2000);
    }

    public void findWareHouses(){
        wareHousesLocations.click();
        whSearchBox.sendKeys("11355");
    }


    public void findWareHousesButton(){
        findWHButton.click();
        //  storeDetailsLocations.click();

    }
    public void verifyWHPage(){
        String wareHouse = wareHousesLocationsLink.getText();
        Assert.assertEquals("Find a Warehouse",wareHouse);

    }
}
