package com.shiftedtech.costco.steps.Steps;

import com.shiftedtech.costco.steps.Base.StepBase;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

/**
 * Created by Mithu on 10/21/2017.
 */
public class RegistrationPageSteps extends StepBase {

    @When("^click on createAccount$")
    public void click_on_createAccount() throws Throwable {
        WebElement element = driver.findElement(By.xpath(".//*[@id='LogonForm']/fieldset/div[8]/a"));
        element.click();
    }

    @Then("^enter \"([^\"]*)\" as email address$")
    public void enter_as_email_address(String arg1) throws Throwable {
        WebElement element = driver.findElement(By.xpath(".//*[@id='register_email1']"));
        element.sendKeys("mithucu2000@yahoo.com");
    }

    @Then("^enter \"([^\"]*)\" as password$")
    public void enter_as_password(String arg1) throws Throwable {
        WebElement element = driver.findElement(By.xpath(".//*[@id='logonPassword']"));
        element.sendKeys("mithuzaw");
    }

    @Then("^enter \"([^\"]*)\" as confirm password$")
    public void enter_as_confirm_password(String arg1) throws Throwable {
        WebElement element = driver.findElement(By.xpath(".//*[@id='logonPasswordVerify']"));
        element.sendKeys("mithuzaw");
    }

    @Then("^enter membershipnumber$")
    public void enter_membershipnumber() throws Throwable {
        WebElement element = driver.findElement(By.xpath(".//*[@id='register_userField2']"));
        element.sendKeys("");
    }

    @Then("^click registraitonbutton$")
    public void click_registraitonbutton() throws Throwable {
       WebElement element = driver.findElement(By.xpath(".//*[@id='RegisterForm']/fieldset/div[7]/input"));
       element.click();
    }


}
