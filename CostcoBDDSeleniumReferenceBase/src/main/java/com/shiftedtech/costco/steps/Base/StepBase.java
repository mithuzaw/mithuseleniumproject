package com.shiftedtech.costco.steps.Base;

import com.shiftedtech.costco.steps.DriverFactory;
import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

/**
 * Created by Mithu on 10/17/2017.
 */

public class StepBase {

 //   protected static WebDriver driver;
    protected WebDriver driver = DriverFactory.getInstance().getDriver();

    //Mouse hover.

    public void mouseOn(WebElement element){
        Actions actions = new Actions(driver);
        actions.moveToElement(element).build().perform();

    }
    //Delay for certain time.

    public void delayFor(int time){
        try {
            Thread.sleep(time);
        }catch (InterruptedException e){
            e.printStackTrace();
        }
    }

   /* //Drop down list
    public void selectOption(WebElement element,String text){
        Select option = new Select(element);
        option.selectByVisibleText(text);
    }*/


    public void verifyPageTitle(String expectedTitle){
        String actualTitle = driver.getTitle();
        Assert.assertEquals(actualTitle,expectedTitle);

    }


}
