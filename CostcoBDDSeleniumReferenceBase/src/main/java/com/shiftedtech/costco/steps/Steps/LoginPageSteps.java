package com.shiftedtech.costco.steps.Steps;

import com.shiftedtech.costco.steps.Base.StepBase;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

/**
 * Created by Mithu on 10/16/2017.
 */
public class LoginPageSteps extends StepBase {

    @When("^Enter \"([^\"]*)\" as email address$")
    public void enter_as_email_address(String arg1) throws Throwable{
        WebElement element = driver.findElement(By.xpath(".//*[@id='logonId']"));
        element.sendKeys(arg1);
    }

    @When("^Enter \"([^\"]*)\" as password$")
    public void enter_as_password(String arg1) throws Throwable {
        WebElement element = driver.findElement(By.xpath(".//*[@id='logonPassword']"));
        element.sendKeys(arg1);
    }

    @When("^Click login$")
    public void click_login() throws Throwable {
        WebElement element = driver.findElement(By.xpath(".//*[@id='LogonForm']/fieldset/div[5]/input"));
        element.click();
    }

    @Then("^Welcome message shows \"([^\"]*)\"$")
    public void welcome_message_shows(String arg1) throws Throwable {
        WebElement element = driver.findElement(By.id("myaccount-d"));
        String text = element.getText();
        Assert.assertEquals(arg1,text);
    }


    @Then("^Error message should display?")
    public void verifyInvalidLoginMessage(){
        WebElement element = driver.findElement(By.id(".critical-notification.form-group"));
        String text = element.getText();
        Assert.assertEquals("The email address and/or password you entered are invalid.",text);

    }
    @When("Login with user \"([^\"]*)\" and password \"([^\"]*)")
    public void login(String user, String password) throws Throwable {
        enter_as_email_address(user);
        enter_as_password(password);
        click_login();


    }
}
