package com.shiftedtech.costco.steps.Base;

import com.shiftedtech.costco.steps.DriverFactory;
import cucumber.api.java.After;
import cucumber.api.java.Before;

import java.util.concurrent.TimeUnit;

/**
 * Created by Mithu on 10/17/2017.
 */
public class BeforeAfterHook extends StepBase {

    @Before
    public void before() {

        // ChromeDriverManager.getInstance().setup();
    // driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.manage().window().maximize();
    }

    @After
    public void after(){
        DriverFactory.getInstance().removeDriver();
     //   driver.close();
       // driver.quit();
        driver = null;

    }

}
