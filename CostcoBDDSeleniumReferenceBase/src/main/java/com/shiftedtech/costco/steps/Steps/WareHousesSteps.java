package com.shiftedtech.costco.steps.Steps;

import com.shiftedtech.costco.steps.Base.StepBase;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.util.List;

/**
 * Created by Mithu on 10/21/2017.
 */
public class WareHousesSteps extends StepBase{

    @When("^mouseon the findwarehouselink$")
    public void mouseon_the_findwarehouselink() throws Throwable {
        WebElement wareHousesLocation = driver.findElement(By.id("warehouse-locations"));
        mouseOn(wareHousesLocation);
    }

    @Then("^the user enters \"([^\"]*)\" in the search field$")
    public void the_user_enters_in_the_search_field(String arg1) throws Throwable {
        WebElement element = driver.findElement(By.id("warehouse-search-field"));
        element.sendKeys("11355");
    }

    @Then("^the user should select \"([^\"]*)\" and \"([^\"]*)\" from Checkbox$")
    public void the_user_should_select_and_from_Checkbox(String arg1, String arg2) throws Throwable {
        List<WebElement> checkbox = driver.findElements(By.cssSelector(".style-check>label>span"));
        for (WebElement element : checkbox) {
            String name = element.getText();
            System.out.println("List of names: " + name);
            if (name.contentEquals("Gas Station")) {
                element.click();
                break;
            }

        }
    }

    @Then("^Click on \"([^\"]*)\"$")
    public void click_on(String arg1) throws Throwable {
        WebElement element = driver.findElement(By.xpath("//input[@value='Find a Warehouse']"));
        element.click();
    }

    @Then("^Verify WareHouse page$")
    public void verify_WareHouse_page() throws Throwable {
       WebElement wareHousesLocationsLink = driver.findElement(By.id("warehouse-locations"));
        String wareHouse = wareHousesLocationsLink.getText();
        Assert.assertEquals("Find a Warehouse",wareHouse);

    }
}
