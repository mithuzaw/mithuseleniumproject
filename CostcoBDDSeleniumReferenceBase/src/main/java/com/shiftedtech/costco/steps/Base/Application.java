package com.shiftedtech.costco.steps.Base;

import com.shiftedtech.costco.steps.Pages.HomePage;
import com.shiftedtech.costco.steps.Pages.LoginPage;
import com.shiftedtech.costco.steps.Pages.RegistrationPage;
import com.shiftedtech.costco.steps.Pages.WareHousesPage;
import org.openqa.selenium.WebDriver;

/**
 * Created by Mithu on 10/21/2017.
 */
public class Application {

    protected WebDriver driver;
    protected HomePage homePage;
    protected RegistrationPage registrationPage;
    protected LoginPage loginPage;
    protected WareHousesPage wareHousesPage;

    /*driver.navigate().to("https://www.costco.com/");
    homePage = new HomePage(driver);*/

    public Application(WebDriver driver) {
        this.driver = driver;
    }

    public HomePage homePage(){
        if (homePage == null){
            homePage = new HomePage(driver);
        }
        return homePage;
    }
    public RegistrationPage registrationPage(){
        if (registrationPage == null){
            registrationPage = new RegistrationPage(driver);
        }
        return registrationPage;
    }
    public LoginPage loginPage(){
        if (loginPage == null){
            loginPage = new LoginPage(driver);
        }
        return loginPage;
    }

    public WareHousesPage wareHousesPage(){
        if (wareHousesPage == null){
            wareHousesPage = new WareHousesPage(driver);
        }
        return wareHousesPage;
    }

}
