Feature: Costco e-commerce WareHouses Functionality.

  Background:
    Given As a not validated user.
    When Browse to the url "https://www.costco.com/"
    Then Costco home page should show

  @debug
  @smoke

  Scenario: 1.  Find Valid Warehouses Address
    When mouseon the findwarehouselink
    And the user enters "11355" in the search field
    And the user should select "Gas Station" and "Tire Center" from Checkbox
    And Click on "FindWareHousesButton"
    And Verify WareHouse page

