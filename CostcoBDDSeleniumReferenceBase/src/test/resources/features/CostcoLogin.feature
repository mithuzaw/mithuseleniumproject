Feature: Costco e-commerce Login Functionality.

  Background:
    Given As a not validated user.
    When Browse to the url "https://www.costco.com/"
    Then Costco home page should show
    When User click login button

  @debug
  @smoke

  Scenario: 1. Valid user Valid password

    And Enter "sandakhain@gmail.com" as email address
    And Enter "sandakhain18" as password
    And Click login
    Then Welcome message shows "My Account "



  Scenario: 2. InValid user Valid password

    And Enter "sandakhainxxxx@gmail.com" as email address
    And Enter "sandakhain18" as password
    And Click login
    And Welcome message shows "My Account"
    Then Error message should display



  Scenario: 3. InValid user InValid password

    And Login with user "sandakhainxxxx@gmail.com" and password "invalid"
    Then Error message should display



  Scenario Outline: 4. Valid user valid password
    And Enter "<emailaddress>" as email address
    And Enter "<password>" as password
    And Click login
    Then Welcome message shows "<username>"


    Examples:
      |emailaddress            |password         |username  |
      |sandakhain@gmail.com    |sandakhain18     |My Account|
      |mithucu2000@yahoo.com   |mithuzaw         |My Account|

