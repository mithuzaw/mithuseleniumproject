package TestPrac;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * Created by Mithu on 5/2/2017.
 */
public class CheckingAccountTest {

    private Account myAccount;

    @Before
    public void setUp(){

        System.out.println("Running setup...");
        myAccount = new CheckingAccount ("998888");

    }

    @Test

    public void test1(){
        System.out.println("Running test1");

        myAccount.deposit(2000);
        Assert.assertEquals(1001,myAccount.getBalance());

    }

    @Test
    public void test2(){


    }

    @After
    public void tearDown(){
        System.out.println("Running teardown");

    }

}
