import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.concurrent.TimeUnit;

/**
 * Created by Mithu on 5/3/2017.
 */
public class MyFirstTestPrac {
    public static void main(String[] args){
        WebDriver driver = null;

        System.setProperty("webdriver.chrome.driver",System.getProperty("user.dir") + "/WebDriver/chromedriver.exe");
        System.out.println("Driver Path:" + System.getProperty("webdriver.chrome.driver"));

        driver = new ChromeDriver();

        driver.manage().window().maximize();

        driver.manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS);
        driver.manage().timeouts().pageLoadTimeout(30,TimeUnit.SECONDS);

        //driver.get("http://spree.shiftedtech.com/");
        driver.navigate().to("http://spree.shiftedtech.com/");
        driver.findElement(By.linkText("Login")).click();


        //WebElement emailElement = driver.findElement(By.id("spree_user_email"));
        WebElement emailElement = driver.findElement(By.id(".//*[@id='spree_user_email']"));
        emailElement.sendKeys("mithucu2000@yahoo.com");
        String userid = emailElement.getAttribute("Value");
        System.out.println("Email entered :" + userid);


        WebElement passwordElement = driver.findElement(By.id("spree_user_password"));
        passwordElement.sendKeys("123456");

        driver.findElement(By.cssSelector(".btn.btn-lg.btn-success.btn-block")).click();

        driver.close();
        driver.quit();

    }
}
