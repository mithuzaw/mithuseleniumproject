import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * Created by Mithu on 5/3/2017.
 * Modified by Raihan.
 * It is a very important class.
 * @return Test Return
 * @param
 *
 */
public class MyFirstUnitTest {
    private WebDriver driver = null;

    @Before
    public void setup() {
        System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir") + "/webDriver/chromedriver.exe");
        System.out.println("Driver Path:" + System.getProperty("webdriver.chrome.driver"));

        driver = new ChromeDriver();
        driver.manage().window().maximize();

        driver.manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS);
        System.out.println("time out implicitlywait: 1 seconds");
        driver.manage().timeouts().pageLoadTimeout(30, TimeUnit.SECONDS);
        System.out.println("time out pageload:30 seconds");


        //driver.get("http://spree.shiftedtech.com/");
        driver.navigate().to("http://spree.shiftedtech.com/");
    }

    @Test
    public void validLoginWithValidEmail() {

        driver.findElement(By.linkText("Login")).click();

        WebElement emailElement = driver.findElement(By.id("spree_user_email"));
        emailElement.sendKeys("mithucu2000@yahoo.com");
        String userid = emailElement.getAttribute("Value");
        System.out.println("Email entered :" + userid);

        WebElement passwordElement = driver.findElement(By.id("spree_user_password"));
        passwordElement.sendKeys("123456");

        driver.findElement(By.cssSelector(".btn.btn-lg.btn-success.btn-block")).click();
        // WebElement username = driver.findElement(By.cssSelector("/logout"));
        //String actualText = username.getText();
        // Assert.assertEquals("Mithu",actualText);

    }
    @Test
    public void validLoginWithInValidEmail() {

        driver.findElement(By.linkText("Login")).click();

        WebElement emailElement = driver.findElement(By.id("spree_user_email"));
        emailElement.sendKeys("mithucu2000cc@yahoo.com");
        String userid = emailElement.getAttribute("Value");
        System.out.println("Email entered :" + userid);

        WebElement passwordElement = driver.findElement(By.id("spree_user_password"));
        passwordElement.sendKeys("123456");

        driver.findElement(By.cssSelector(".btn.btn-lg.btn-success.btn-block")).click();

        //WebElement username = driver.findElement(By.cssSelector("/logout"));
        //String actualText = username.getText();
        // Assert.assertEquals("Mithu",actualText);
    }
    @Test
    public void validLoginWithInValidEmailInvalidPassword() {

        driver.findElement(By.linkText("Login")).click();

        WebElement emailElement = driver.findElement(By.id("spree_user_email"));
        emailElement.sendKeys("mithucu2000cc@yahoo.com");
        String userid = emailElement.getAttribute("Value");
        System.out.println("Email entered :" + userid);

        WebElement passwordElement = driver.findElement(By.id("spree_user_password"));
        passwordElement.sendKeys("654321");

        driver.findElement(By.cssSelector(".btn.btn-lg.btn-success.btn-block")).click();
    }

    @Test
    public void ValidEmailPasswordWithLogout(){
        driver.findElement(By.linkText("Login")).click();

        WebElement emailElement = driver.findElement(By.id("spree_user_email"));
        emailElement.sendKeys("mithucu2000@yahoo.com");
        String userid = emailElement.getAttribute("Value");
        System.out.println("Email entered :" + userid);

        WebElement passwordElement = driver.findElement(By.id("spree_user_password"));
        passwordElement.sendKeys("123456");

        driver.findElement(By.cssSelector(".btn.btn-lg.btn-success.btn-block")).click();

        driver.findElement(By.linkText("Logout")).click();
    }
    @Test
    public void EmailEmptyPasswordLogin(){
        driver.findElement(By.linkText("Login")).click();

        WebElement emailElement = driver.findElement(By.id("spree_user_email"));
        emailElement.sendKeys("mithucu2000@yahoo.com");
        String userid = emailElement.getAttribute("Value");
        System.out.println("Email entered :" + userid);

        WebElement passwordElement = driver.findElement(By.id("spree_user_password"));
        passwordElement.sendKeys("");

        driver.findElement(By.cssSelector(".btn.btn-lg.btn-success.btn-block")).click();
        driver.findElement(By.linkText("Login")).click();

    }
    @Test
    public void WithoutEmailValidPasswordLogin(){
        driver.findElement(By.linkText("Login")).click();

        WebElement emailElement = driver.findElement(By.id("spree_user_email"));
        emailElement.sendKeys("");
        String userid = emailElement.getAttribute("Value");
        System.out.println("Email entered :" + userid);

        WebElement passwordElement = driver.findElement(By.id("spree_user_password"));
        passwordElement.sendKeys("123456");

        driver.findElement(By.cssSelector(".btn.btn-lg.btn-success.btn-block"));
        driver.findElement(By.linkText("Login")).click();
    }
    @Test
    public void WithoutEmailAndPasswordLogin(){
        driver.findElement(By.linkText("Login")).click();

        WebElement emailElement = driver.findElement(By.id("spree_user_email"));
        emailElement.sendKeys("");
        String userid = emailElement.getAttribute("Value");
        System.out.println("Email entered :" + userid);

        WebElement passwordElement = driver.findElement(By.id("spree_user_password"));
        passwordElement.sendKeys("");

        driver.findElement(By.cssSelector(".btn.btn-lg.btn-success.btn-block")).click();

        driver.findElement(By.linkText("Login")).click();
    }

    @Test
    public void addCart() {
        driver.findElement(By.linkText("Login")).click();

        WebElement emailElement = driver.findElement(By.name("spree_user[email]"));
        emailElement.sendKeys("mithucu2000@yahoo.com");
        String userid = emailElement.getAttribute("Value");
        System.out.println("Email entered :" + userid);

        WebElement passwordElement = driver.findElement(By.id("spree_user_password"));
        passwordElement.sendKeys("123456");


        driver.findElement(By.cssSelector(".btn.btn-lg.btn-success.btn-block")).click();

        driver.findElement(By.xpath(".//*[@id='product_1']//a[text()='Ruby on Rails Tote']")).click();
        driver.findElement(By.xpath(".//*[@id='add-to-cart-button']")).click();
        driver.findElement(By.cssSelector(".btn.btn-lg.btn-success")).click();
        driver.findElement(By.linkText("Logout"));

    }
    @Test
    public void checkout(){

        driver.findElement(By.linkText("Login")).click();

        WebElement emailElement = driver.findElement(By.id("spree_user_email"));
        emailElement.sendKeys("mithucu2000@yahoo.com");
        String userid = emailElement.getAttribute("Value");
        System.out.println("Email entered :" + userid);

        WebElement passwordElement = driver.findElement(By.id("spree_user_password"));
        passwordElement.sendKeys("123456");

        driver.findElement(By.cssSelector(".btn.btn-lg.btn-success.btn-block")).click();

        driver.findElement(By.xpath(".//*[@id='product_1']//a[text()='Ruby on Rails Tote']")).click();
        driver.findElement(By.xpath(".//*[@id='add-to-cart-button']")).click();
        driver.findElement(By.cssSelector(".btn.btn-lg.btn-success")).click();
        driver.findElement(By.linkText("Logout"));

        driver.findElement(By.xpath(".//*[@id='order_email']"));



       // WebElement customerEmailElement = driver.findElement(By.xpath(".//*[@id='order_email']"));
      //  customerEmailElement.sendKeys("mithucu2000@yahoo.com");
       // String userid = customerEmailElement.getAttribute("Value");
        //System.out.println("Email entered :" + userid);

        WebElement firstNameElement = driver.findElement(By.xpath(".//*[@id='order_bill_address_attributes_firstname']"));
        firstNameElement.clear();
        firstNameElement.sendKeys("Mithu");

        WebElement lastNameElement = driver.findElement(By.xpath(".//*[@id='order_bill_address_attributes_lastname']"));
        lastNameElement.clear();
        lastNameElement.sendKeys("Mitu Last");

        WebElement streetAddressElement = driver.findElement(By.id("order_bill_address_attributes_address1"));
        streetAddressElement.clear();
        streetAddressElement.sendKeys("42  Judge Street,");

        WebElement streetAddress2Element = driver.findElement(By.id("order_bill_address_attributes_address2"));
        streetAddress2Element.sendKeys("APT 6");


        WebElement cityElement = driver.findElement(By.xpath(".//*[@id='order_bill_address_attributes_city']"));
        cityElement.clear();
        cityElement.sendKeys("New York");

        WebElement countryDropdown = driver.findElement(By.xpath(".//*[@id='order_bill_address_attributes_country_id']"));
        countryDropdown.click();

        List<WebElement> countryList = countryDropdown.findElements(By.xpath("./option"));

        for (int i =0; i < countryList.size(); i++){
            if(countryList.get(i).getText().contentEquals("United State of America")){
                System.out.println(countryList.get(i).getText());
                countryList.get(i).click();
                break;
            }
        }

        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }


        WebElement stateDropdown = driver.findElement(By.xpath(".//*[@id='order_bill_address_attributes_state_id']"));
        stateDropdown.click();

        List<WebElement> stateList = stateDropdown.findElements(By.xpath("./option"));

        for (int i = 0;i< stateList.size();i++){
            System.out.println(stateList.get(i).getText());
            if (stateList.get(i).getText().contentEquals("New York")){
                stateList.get(i).click();
                break;
            }
        }

        WebElement zipElement = driver.findElement(By.xpath(".//*[@id='order_bill_address_attributes_zipcode']"));
        zipElement.clear();
        zipElement.sendKeys("07105");

        WebElement phoneElement = driver.findElement(By.xpath(".//*[@id='order_bill_address_attributes_phone']"));
        phoneElement.clear();
        phoneElement.sendKeys("6096724944");

      driver.findElement(By.xpath(".//*[@id='checkout_form_address']/div[3]/input")).click();
      driver.findElement(By.xpath(".//*[@id='methods']/div/ul/li[6]"));
      driver.findElement(By.xpath(".//*[@id='checkout_form_delivery']/div[2]/input")).click();
      driver.findElement(By.xpath(".//*[@id='payment-method-fields']/li[1]"));

      WebElement nameOnCardElement = driver.findElement(By.xpath(".//*[@id='name_on_card_1']"));
      nameOnCardElement.clear();
      nameOnCardElement.sendKeys("Mithu");

      WebElement nameCardNumberElement = driver.findElement(By.xpath(".//*[@id='card_number']"));
      nameCardNumberElement.clear();
      nameCardNumberElement.sendKeys("12344567890");

      WebElement cardExpityElement = driver.findElement(By.xpath(".//*[@id='card_expiry']"));
      cardExpityElement.sendKeys("1020");

      WebElement cardCodeElement = driver.findElement(By.xpath(".//*[@id='card_code']"));
      cardCodeElement.sendKeys("123");

      WebElement couponCodeElement = driver.findElement(By.xpath(".//*[@id='order_coupon_code']"));
      couponCodeElement.sendKeys("11");

      driver.findElement(By.xpath(".//*[@id='checkout_form_payment']/div[2]/input")).click();



    }

    @After
    public void teardown(){
       // driver.close();
        //driver.quit();
    }
}

