package com.shiftedtech.spree.steps;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

/**
 * Created by Mithu on 7/23/2017.
 */
public class HomePageSteps {
    private WebDriver driver;

    @Given("^As a not validated user$")
    public void as_a_not_validated_user() throws Throwable {
        driver.manage().deleteAllCookies();
    }

    @When("^Browse to the url \"([^\"]*)\"$")
    public void browse_to_the_url(String arg1) throws Throwable {
        driver.navigate().to(arg1);
    }

    @Then("^Spree home page should show$")
    public void spree_home_page_should_show() throws Throwable {

        //   Thread.sleep(3000);

        WebElement element = driver.findElement(By.xpath(".//*[@id='home-link']/a"));
        String actual = element.getText();
        Assert.assertEquals("Home",actual);
    }

    @When("^User click login button$")
    public void user_click_login_button() throws Throwable {
        WebElement element =  driver.findElement(By.linkText("Login"));
        element.click();

    }


    @When("^Welcome message shows \"([^\"]*)\"$")
    public void welcome_message_shows(String arg1) throws Throwable {
        WebElement element = driver.findElement(By.xpath(".//*[@id='nav-bar']/li[1]/a"));
        String text = element.getText();
        Assert.assertEquals(arg1,text);
    }

}
