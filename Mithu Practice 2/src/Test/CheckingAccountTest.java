package Test;

import com.shiftedtechpractice2.com.shiftedtech.bankM2.Account;
import com.shiftedtechpractice2.com.shiftedtech.bankM2.CheckingAccountPrac2;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * Created by Mithu on 5/2/2017.
 */
public class CheckingAccountTest {

    private Account myAccount;

    @Before
    public void setUp(){
        myAccount = new CheckingAccountPrac2("567889999");

    }

    @Test
    public void depositTest(){
        System.out.println("Running Deposit Test***********");
        myAccount.deposit(1000);
        Assert.assertEquals(1001,myAccount.getBalance(),0);

    }

    @Test
    public void accountNumber(){
        String expectedAccountNumber = "567889999";
        Assert.assertEquals(expectedAccountNumber,myAccount.getAccountNumber());
    }

    @Test
    public void firstName(){
        String expectedfirstName = "Mithu";
        myAccount.setFirstName("Mithu");
        Assert.assertEquals(expectedfirstName,myAccount.getFirstName());
    }

    @Test
    public void lastName(){
        String expectedlastname = "Zaw";
        myAccount.setLastName("Zaw");
        Assert.assertEquals(expectedlastname,myAccount.getLastName());
    }
    @Test
    public void WithdrawTest1(){
        System.out.println("Running Withdraw Test 1*************");
        myAccount.deposit(1000);
        myAccount.withdraw(500);
        Assert.assertEquals(501,myAccount.getBalance(),0);
    }
    @Test
    public void WithdrawTest2(){
        System.out.println("Running Withdraw Test 2***********");
        myAccount.deposit(1000);
        myAccount.withdraw(400);
        Assert.assertEquals(600,myAccount.getBalance(),0);
    }

    @After
    public void tearDown(){
        System.out.println("teardown");
        myAccount = null;

    }
}
