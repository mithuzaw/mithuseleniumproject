package com.shiftedtechpractice2;

import java.util.Vector;

/**
 * Created by Mithu on 4/21/2017.
 */
public class VectorProgramPrac2 {
    public static void main(String[] args) {

        Vector<String> names = new Vector<>();
        names.add("Honey");
        names.addElement("Zinia");
        names.add("Ale");

        System.out.println("Count : " + names.size());
        String item = names.get(1);
        System.out.println("Item: " + item);
    }
}
