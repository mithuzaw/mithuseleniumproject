package com.shiftedtechpractice2;

/**
 * Created by Mithu on 4/18/2017.
 */
public class StringTestProgramM {
    public static void main(String[] args) {
        System.out.println("************String test*******");

        String myName = " Mithu Khin Zaw ";
        String expected = "MITHU KHIN ZAW ";

        if (myName.toUpperCase().contentEquals(expected)){
            System.out.println("It match");
        }
        else {
            System.out.println("It does not match");
        }
        System.out.println("************* not to comparison string like below. two different contain *****");

        if (myName.toUpperCase()==expected) {
            System.out.println("It Match");
        }else {
            System.out.println("It doesn't match");
        }
        System.out.println("********same method as below**********");

        System.out.println("MyName :" + myName);
        myName.toUpperCase();
        myName = myName.toUpperCase().trim();
        System.out.println("After toUpper MyName : " + myName);

        System.out.println("************** to remove begin space triling space, not middle space*");

        if (myName.trim().toUpperCase().contentEquals(expected.trim())){
            System.out.println("IT MATCH");
        }else {
            System.out.println("IT DOES NOT MATCH");
        }
        System.out.println(myName.startsWith("MITHU"));
        System.out.println(myName.endsWith("ZAW"));
        System.out.println(myName.contains("KHIN"));
        System.out.println(myName.contains("MITHU"));

        System.out.println("to know string begining with how many char");

        int start = myName.indexOf(" ");
      //  int end = myName.indexOf(" ",start+1);
        int end = myName.lastIndexOf("");
        System.out.println("First space:" + start);
        System.out.println("Second space:" + end);
        //System.out.println(myName.substring(6,10));
        System.out.println(myName.substring(start+1,end));

        System.out.println("Overloaded method in substring");
        System.out.println(myName.substring(6));

        System.out.println("To know how long the length is ");

        System.out.println("Length : " + myName.length());
        System.out.println("Convert char into code");

        char[] charArray = myName.toCharArray();
        for (int i = 0;i<charArray.length; i++){
            int code = charArray[i];
            System.out.print(charArray[i] + ":" + code);
            System.out.println();
        }
        System.out.println("convert code in to char");

        StringBuilder sb = new StringBuilder();
        for (int i = charArray.length-1;i>=0;i--){
            sb.append(charArray[i]);
        }
        System.out.println(sb.toString());

    }
}
