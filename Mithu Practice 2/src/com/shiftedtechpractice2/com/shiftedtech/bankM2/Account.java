package com.shiftedtechpractice2.com.shiftedtech.bankM2;

/**
 * Created by Mithu on 4/19/2017.
 */
public abstract class Account implements IAccount,ICCard {


    private String accountNumber;
    private String firstName;
    private String lastName;
    private double balance;

    private Account() {
        System.out.println("Creating account object");
    }

    public Account(String accountNumber) {
        System.out.println("Creating account object");
        setAccountNumber(accountNumber);
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        if (accountNumber.length() >= 6) {
            this.accountNumber = accountNumber;
        } else {
            System.out.println("Please provide valid number which is atleast 6 char");
        }
    }

    public double getBalance() {
        return balance;
    }

    public void deposit(double amount) {
        balance = balance + amount + getReward();
    }

    public void withdraw(double amount) {
        System.out.println("Withdraw amount :" + amount);
        if (amount <= balance) {
            balance = balance - amount;
        } else {
            System.out.println("you can not withdraw amount :" + amount);
            System.out.println("you don't have enough money");

        }
    }

    public void swipe() {
        System.out.println("I am swiping the card.");
    }

    public abstract double getReward();

}
