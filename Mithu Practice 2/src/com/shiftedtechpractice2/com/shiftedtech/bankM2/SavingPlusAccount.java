package com.shiftedtechpractice2.com.shiftedtech.bankM2;

/**
 * Created by Mithu on 4/28/2017.
 */
public class SavingPlusAccount extends SavingAccountPrac2 {

    public SavingPlusAccount(String accountNumber) {
        super(accountNumber);}

        @Override
        public double getReward() {
            return 20;
    }

}
