package com.shiftedtechpractice2.com.shiftedtech.bankM2;

/**
 * Created by Mithu on 4/22/2017.
 */
public class SavingAccountPrac2 extends Account {
    public SavingAccountPrac2(String accountNumber) {
        super(accountNumber);
    }

    @Override
    public double getReward() {
        return 10;
    }
    public double getInterest(){
        double amount ;

        amount = getBalance()* .10;
        return amount;
    }
}
