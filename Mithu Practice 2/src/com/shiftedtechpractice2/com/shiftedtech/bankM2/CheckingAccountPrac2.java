package com.shiftedtechpractice2.com.shiftedtech.bankM2;

/**
 * Created by Mithu on 4/22/2017.
 */
public class CheckingAccountPrac2 extends Account {

    public CheckingAccountPrac2(){
        super("99999999");
    }

    public CheckingAccountPrac2(String accountNumber){
        super(accountNumber);
        System.out.println("Creating Checking account object");
    }

    @Override
    public void withdraw(double amount) {
        super.withdraw(amount);
        if (amount < 500) {
            super.withdraw(1);
        }
    }

    @Override
    public double getReward() {
        return 1;
    }

    public void withdraw(double amount, String msg) {
        System.out.println("Msg :" + msg);
        super.withdraw(amount);
        if (amount < 500) {
            System.out.println("Withdraw Charge : $1.00");
            super.withdraw(1);
        }
    }

    public void withdraw(String msg, double amount) {
        System.out.println("Msg :" + msg);
        super.withdraw(amount);
        if (amount < 500) {
            super.withdraw(2);
        }
    }
}

