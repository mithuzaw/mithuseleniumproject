package com.shiftedtechpractice2.com.shiftedtech.bankM2;

/**
 * Created by Mithu on 4/28/2017.
 */
public class MyBank2 {
    public static void main(String[] args) {

        SavingAccountPrac2 myAccount = new SavingAccountPrac2("7788999877");
        // Account myAccount = new SavingAccountPrac2("6678886665");
        depositMoney((Account) myAccount, 1000);
        printStatement(myAccount);
        PrintInterest(myAccount);
        swipeCard(myAccount);


        if (myAccount instanceof SavingAccountPrac2) {
            System.out.println(((SavingAccountPrac2) myAccount).getReward());
        }
        //((SavingAccountPrac2)myAccount).getInterest();
        // ((CheckingAccountPrac2)myAccount).getReward();
        //myAccount.getInterest();

        //Account yourAccount = new SavingAccountPrac2("6667778888");

        //CheckingAccountPrac2 myCheckingAccount = new CheckingAccountPrac2("7788888");
        Account myCheckingAccount = new CheckingAccountPrac2("8899977666");
        depositMoney(myCheckingAccount, 1000);
        printStatement(myCheckingAccount);
        PrintInterest(myCheckingAccount);
        swipeCard(myCheckingAccount);

        IAccount mySavingPlusAccount = new SavingPlusAccount("8899999");

    }



    public static void depositMoney(IAccount accountPrac2,double amount){
        System.out.println("Depositing money for account: " + ((Account)accountPrac2).getAccountNumber());
        System.out.println("Amount :" + amount);
        accountPrac2.deposit(amount);
    }
    public static void printStatement(IAccount accountPrac2){
        System.out.println("your statement balance is for account " + ((Account)accountPrac2).getAccountNumber());
        System.out.println(accountPrac2.getBalance());}


    public static void PrintInterest(Account account){
        if (account instanceof SavingAccountPrac2){
            System.out.println("Interest :" + ((SavingAccountPrac2)account).getInterest());
        }
        else
        {
            System.out.println("You do not get any interest.");
        }

    }
    public static void swipeCard(ICCard card){
        card.swipe();
    }
}
