package com.shiftedtechpractice2.com.shiftedtech.bankM2;

/**
 * Created by Mithu on 4/19/2017.
 */
public class MyBank {
    public static void main(String[] args) {
        System.out.println("*******MY Bank************");

        Account myAccount = new SavingAccountPrac2("3890998880");

        //myAccount.setAccountNumber("3890998880");
        myAccount.setFirstName("Honey");
        myAccount.setLastName("Win");

        myAccount.deposit(1000);
        myAccount.withdraw(420);
       // myAccount.withdraw(1000);


        //myAccount.balance = 10000;

        System.out.println("My balance: " + myAccount.getBalance());

        Account yourAccount = new SavingAccountPrac2("2399999002");
      //  yourAccount.setAccountNumber("2399999002");
        yourAccount.setFirstName("Maung");
        yourAccount.setLastName("Zaw");

        yourAccount.deposit(2000);
        yourAccount.withdraw(100);

        System.out.println("Your balance:" + yourAccount.getBalance());


        System.out.println("*******Checking Account ****************");

        CheckingAccountPrac2 myCAccount = new CheckingAccountPrac2("7789999777");
       // myCAccount.setAccountNumber("7789999777");
        myCAccount.setFirstName("Honey");
        myCAccount.setLastName("Zaw");
        myCAccount.deposit(2000);
        myCAccount.withdraw(300,"for shift payment");
        System.out.println("Your balance: " + myCAccount.getBalance() );


    }
}
