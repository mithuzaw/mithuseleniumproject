package com.shiftedtechpractice2.com.shiftedtech.bankM2;

/**
 * Created by Mithu on 4/29/2017.
 */
public interface IAccount {
    void deposit(double amount);
    public abstract void withdraw(double amount);
    public double getBalance();

}
