package com.shiftedtechpractice2;

import java.util.Iterator;
import java.util.Stack;

/**
 * Created by Mithu on 4/21/2017.
 */
public class StackProgramPrac2 {
    public static void main(String[] args) {
        Stack<String> st = new Stack<>();

        st.push("Honey");
        st.push("Zinia");
        st.push("Ale");

        System.out.println("Count:" + st.size());

        System.out.println("********to know Current item*******");

       // String name = st.peek();
        String name = st.pop();
        System.out.println("Name :" + name);
        System.out.println("Count:" + st.size());

       // st.clear();
        System.out.println("Count :" + st.size());

       // while (st.size()> 0){
          //  System.out.println("Name:" + st.pop());
      //  }
        Iterator<String> it = st.iterator();
        while (it.hasNext())
        {
            System.out.println("Name :" + it.next());
        }
        System.out.println("Count :" +st.size());
    }
}
