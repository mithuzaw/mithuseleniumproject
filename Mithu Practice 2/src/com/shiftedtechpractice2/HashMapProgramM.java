package com.shiftedtechpractice2;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * Created by Mithu on 4/21/2017.
 */
public class HashMapProgramM {
    public static void main(String[] args) {

        HashMap<String,Integer>grades = new HashMap<>();

        grades.put(null,00);
        grades.put("Honey", 99);
        grades.put("Zinia", 97);
        grades.put("Shantu",90);

        System.out.println("count" + grades.size());

        System.out.println("**************** if the key exist or not");

        String myKey = "Honey";
        if (grades.containsKey(myKey)){
            Integer Grade = grades.get(myKey);
            System.out.println("Honey :" + Grade);
        }
        else {
            System.out.println("Key" + myKey + "Does not exist");
        }

        System.out.println("************* to know the specific person number********");

        Map<String,String> Phonebook = new HashMap<>();

        Phonebook.put("Mithu" ," 01819310148");
        Phonebook.put("Supreya" , "01819885306");
        Phonebook.put("U Thin Khin" , "01817310148");
        Phonebook.put("Maung K Zaw" , "01817058089");

        System.out.println(Phonebook.get("Mithu"));

       /* System.out.println("*************Interface inside the interface******");

        Set<Map.Entry<String,String>> values = Phonebook.entrySet();
        for (Map.Entry<String,String> e: values)
        {
            System.out.println(e.getKey()+ ":" + e.getKey());
            e.setValue("III");
        }*/

        System.out.println("*************Phone list***************");

        Set<String> keys = Phonebook.keySet();
        for (String i : keys)
        {
            System.out.println(i + ":" + Phonebook.get(i));
        }

    }
}
