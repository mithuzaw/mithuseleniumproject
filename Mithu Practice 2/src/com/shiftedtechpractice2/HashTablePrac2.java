package com.shiftedtechpractice2;

import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Set;

/**
 * Created by Mithu on 4/10/2017.
 */
public class HashTablePrac2 {
    public static void main(String[] args) {
        System.out.println("************* to find out specific student's grade");
        Hashtable<String, Integer> classgrade = new Hashtable<>();
        classgrade.put("Honey", 99);
        classgrade.put("Zinia", 97);
        classgrade.put("Ale", 96);
        classgrade.put("Maria", 98);


        Integer grade = classgrade.get("Honey");
        System.out.println("Honey :" + grade);


        System.out.println("*********to know total student number*********");

        System.out.println("Count :" + classgrade.size());

        System.out.println("**************** if the key exist or not");

        String myKey = "Honey";
        if (classgrade.containsKey(myKey)){
          Integer Grade = classgrade.get(myKey);
            System.out.println("Honey :" + Grade);
        }
        else
        {
            System.out.println("Key" + myKey + "Does not exist");
           // throw new RuntimeException("Key" + myKey + "doesn't exist")
        }

        System.out.println("***************** if the value exist");
        if (classgrade.contains(99));
        System.out.println("It has 99");

        System.out.println("*************** if need to iterate and need to know how many key");

        Enumeration<String> keys = classgrade.keys();
        while (keys.hasMoreElements()){
            String key = keys.nextElement();
            System.out.println("Key :" + key + " has value " + classgrade.get(key));
        }

        System.out.println("****************** for iteration");

        Set<String> keys2 = classgrade.keySet();
        for (String key:keys2){
            System.out.println("Key :" + key + " has value " +classgrade.get(key));
        }

        System.out.println("************* if need to clear everything");
        classgrade.clear();
        System.out.println("Count :" + classgrade.size());
    }
}
