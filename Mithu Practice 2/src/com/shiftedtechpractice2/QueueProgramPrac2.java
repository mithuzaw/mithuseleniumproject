package com.shiftedtechpractice2;

import java.util.LinkedList;
import java.util.Queue;

/**
 * Created by Mithu on 4/21/2017.
 */
public class QueueProgramPrac2 {
    public static void main(String[] args) {

        Queue <String>myQueue = new LinkedList();
        myQueue.add("Honey");
        myQueue.add("Zinia");
        myQueue.add("Ale");

        System.out.println("Count :" + myQueue.size());
        System.out.println("Top of the myQueue :" + myQueue.peek());

        String item = myQueue.poll();
        System.out.println("Item :" + item);

        System.out.println("Count :" + myQueue.size());
        System.out.println("Top of the myQueue :" + myQueue.peek());


    }
}
