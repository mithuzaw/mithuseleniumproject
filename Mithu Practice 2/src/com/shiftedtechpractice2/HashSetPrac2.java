package com.shiftedtechpractice2;

import java.util.HashSet;
import java.util.Iterator;

/**
 * Created by Mithu on 4/21/2017.
 */
public class HashSetPrac2 {
    public static void main(String[] args) {

        HashSet<String> students = new HashSet<>();

        students.add("Honey");
        students.add("Zinia");
        students.add("Ale");
        students.add("Honey");

        System.out.println("Count:" + students.size());

        Iterator<String> items = students.iterator();
        while (items.hasNext()){
            System.out.println("Item :" + items.next());
        }
        students.remove("Ale");
        Iterator<String> items2 = students.iterator();
        while (items2.hasNext()){
            System.out.println("Item 2 :" + items2.next());
        }

    }
}
