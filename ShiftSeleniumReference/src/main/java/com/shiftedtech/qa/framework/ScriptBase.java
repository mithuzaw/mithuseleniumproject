package com.shiftedtech.qa.framework;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.concurrent.TimeUnit;

/**
 * Created by Mithu on 5/6/2017.
 */
public class ScriptBase {
    private WebDriver driver = null;

    @Before
    public void setup(){
        //System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir") + "/webDriver/chromedriver");
        //System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir") +  "/driver/chromedriver");
       System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir") +  "/chromedriver.exe");
        System.out.println("Driver Path:" + System.getProperty("webdriver.chrome.driver"));

        driver = new ChromeDriver();

        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS);
        driver.manage().timeouts().pageLoadTimeout(30,TimeUnit.SECONDS);
        //driver.get("http://heatclinic.shiftedtech.com");
        driver.navigate().to("http://heatclinic.shiftedtech.com");
    }

    @After
    public void tearDown(){
       // driver.close();
        //driver.quit();
    }
    @Test
    public void registerElement() {
        driver.findElement(By.xpath(".//*[@id='cart_info']/a[2]/span")).click();

        WebElement emailElement = driver.findElement(By.xpath(".//*[@id='customer.emailAddress']"));
        emailElement.sendKeys("sandakhain@gmail.com");

        WebElement firstNameElement = driver.findElement(By.xpath(".//*[@id='customer.firstName']"));
        firstNameElement.sendKeys("Mithu");

        WebElement lastNameElement = driver.findElement(By.xpath(".//*[@id='customer.lastName']"));
        lastNameElement.sendKeys("Zaw");

        driver.findElement(By.xpath(".//*[@id='password']")).sendKeys("123456");

        WebElement confirmPassWordElement = driver.findElement(By.xpath(".//*[@id='passwordConfirm']"));
        confirmPassWordElement.sendKeys("123456");

        driver.findElement(By.xpath(".//*[@id='registrationForm']/div[6]/input")).click();
    }

    @Test
    public void productElement(){
            driver.findElement(By.xpath(".//*[@id='content']/nav/ul/li[2]/a")).click();
            driver.findElement(By.className("addToCart")).click();
            driver.findElement(By.className("addToCart")).click();
            driver.findElement(By.id("cartLink")).click();

           driver.findElement(By.xpath(".//*[@id='cart_products']/thead/tr/th[2]")).sendKeys(Keys.UP);
            driver.findElement(By.xpath(".//*[@id='checkout']/a")).click();


        }
    }

