import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.rules.Timeout;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import sun.security.krb5.internal.ktab.KeyTab;

import java.util.concurrent.TimeUnit;

/**
 * Created by Mithu on 5/7/2017.
 */
public class ShiftTest {
    public static WebDriver driver = null;

    @BeforeClass
    public static void beforeClass() {
        System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir") + "/chromedriver.exe");
        System.out.println("Driver Path:" + System.getProperty("webdriver.chrome.driver"));

        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS);
        driver.manage().timeouts().pageLoadTimeout(30, TimeUnit.SECONDS);
    }


    @Before

     public void setUp(){
            driver.navigate().to("http://shifttest.shiftedtech.com/components/text_box");
    }

    @Test
    public void htmlTextBoxElement(){
         WebElement userNameElement = driver.findElement(By.id("username-text"));
         userNameElement.sendKeys("username");

        WebElement passWordElement = driver.findElement(By.id("password-text"));
        passWordElement.sendKeys("password");

        WebElement dateElement = driver.findElement(By.id("date"));
        dateElement.sendKeys("05072017");

        WebElement dateTimeElement = driver.findElement(By.id("datetime-local"));
        dateTimeElement.sendKeys("05072012");
        dateTimeElement.sendKeys(Keys.TAB);
        dateTimeElement.sendKeys("0210PM");


        driver.findElement(By.id("number")).sendKeys(Keys.UP,Keys.UP);
        driver.findElement(By.id("file")).click();

    }
    @Test
    public void bootStrapCssElement(){
        WebElement plainTextBoxElement = driver.findElement(By.id("plain-text"));
        plainTextBoxElement.sendKeys("shiftedtext");

        driver.findElement(By.id("password")).sendKeys("123456");
        driver.findElement(By.id("email")).sendKeys("mithucu2000@yahoo.com");
        driver.findElement(By.id("url")).sendKeys("tutorialspoint.com");
        driver.findElement(By.xpath(".//*[@id='date' and @class='form-control']")).sendKeys("05092017");
        WebElement dateTimeElement = driver.findElement(By.xpath(".//*[@id='datetime-local' and @class='form-control']"));
       dateTimeElement.sendKeys("05092017");
       dateTimeElement.sendKeys(Keys.TAB);
       dateTimeElement.sendKeys("1204AM");

       driver.findElement(By.xpath(".//*[@id='number' and @class='form-control']")).sendKeys("6096724944");
       driver.findElement(By.xpath(".//*[@id='bootstrap-form']/div[8]/label")).click();
       driver.findElement(By.xpath(".//*[@id='range']")).sendKeys(Keys.LEFT);


    }





    }










