import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.concurrent.TimeUnit;

/**
 * Created by Mithu on 4/30/2017.
 */
public class MyFirstUnitTest {

    private WebDriver driver = null;

    @Before
    public void setup() {
        System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir") + "/WebDriver/chromedriver.exe");
        System.out.println("Driver Path:" + System.getProperty("webdriver.chrome.driver"));

        driver = new ChromeDriver();
        driver.manage().window().maximize();

        driver.manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS);
        driver.manage().timeouts().pageLoadTimeout(30, TimeUnit.SECONDS);


        //driver.get("http://spree.shiftedtech.com/");
        driver.navigate().to("http://spree.shiftedtech.com/");
    }

    @Test
    public void validLoginWithValidEmail() {

        driver.findElement(By.linkText("Login")).click();

        WebElement emailElement = driver.findElement(By.id("spree_user_email"));
        emailElement.sendKeys("mithucu2000@yahoo.com");
        String userid = emailElement.getAttribute("Value");
        System.out.println("Email entered :" + userid);

        WebElement passwordElement = driver.findElement(By.id("spree_user_password"));
        passwordElement.sendKeys("123456");

        driver.findElement(By.cssSelector(".btn.btn-lg.btn-success.btn-block")).click();
       // WebElement username = driver.findElement(By.cssSelector("/logout"));
        //String actualText = username.getText();
       // Assert.assertEquals("Mithu",actualText);

    }
    @Test
    public void validLoginWithInValidEmail() {

        driver.findElement(By.linkText("Login")).click();

        WebElement emailElement = driver.findElement(By.id("spree_user_email"));
        emailElement.sendKeys("mithucu2000cc@yahoo.com");
        String userid = emailElement.getAttribute("Value");
        System.out.println("Email entered :" + userid);

        WebElement passwordElement = driver.findElement(By.id("spree_user_password"));
        passwordElement.sendKeys("123456");

        driver.findElement(By.cssSelector(".btn.btn-lg.btn-success.btn-block")).click();

        //WebElement username = driver.findElement(By.cssSelector("/logout"));
        //String actualText = username.getText();
       // Assert.assertEquals("Mithu",actualText);
    }
    @Test
    public void validLoginWithInValidEmailInvalidPassword() {

        driver.findElement(By.linkText("Login")).click();

        WebElement emailElement = driver.findElement(By.id("spree_user_email"));
        emailElement.sendKeys("mithucu2000cc@yahoo.com");
        String userid = emailElement.getAttribute("Value");
        System.out.println("Email entered :" + userid);

        WebElement passwordElement = driver.findElement(By.id("spree_user_password"));
        passwordElement.sendKeys("654321");

        driver.findElement(By.cssSelector(".btn.btn-lg.btn-success.btn-block")).click();
    }

    @Test
    public void ValidEmailPasswordWithLogout(){
        driver.findElement(By.linkText("Login")).click();

        WebElement emailElement = driver.findElement(By.id("spree_user_email"));
        emailElement.sendKeys("mithucu2000@yahoo.com");
        String userid = emailElement.getAttribute("Value");
        System.out.println("Email entered :" + userid);

        WebElement passwordElement = driver.findElement(By.id("spree_user_password"));
        passwordElement.sendKeys("123456");

        driver.findElement(By.cssSelector(".btn.btn-lg.btn-success.btn-block")).click();

        driver.findElement(By.linkText("/logout")).click();
    }

    @Test
    public void CreatNewAccount() {

        driver.findElement(By.linkText("/signup")).click();

        WebElement emailElement = driver.findElement(By.name("spree_user[email]"));
        emailElement.sendKeys("sandakhain@gmail.com");
        String userid = emailElement.getAttribute("Value");
        System.out.println("Email entered :" + userid);
        WebElement passwordElement = driver.findElement(By.name("password-credentials"));
        passwordElement.sendKeys("654321");
        WebElement confirmpasswordElement = driver.findElement(By.name("spree_user[password_confirmation]"));
        confirmpasswordElement.sendKeys("654321");

        driver.findElement(By.cssSelector(".btn.btn-lg.btn-success.btn-block")).click();

            driver.close();
            driver.quit();
        }
    }


