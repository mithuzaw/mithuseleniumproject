import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.concurrent.TimeUnit;

/**
 * Created by Mithu on 4/29/2017.
 */
public class MyFirstTest {

    //http://www.seleniumhq.org/

    public static void main(String[] args) {
        System.out.println("***********Starting test**********");

        WebDriver driver = null;

        System.setProperty("webdriver.chrome.driver",System.getProperty("user.dir") + "/WebDriver/chromedriver.exe");

        driver = new ChromeDriver();

        driver.manage().window().maximize();

        driver.manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS);
        driver.manage().timeouts().pageLoadTimeout(30,TimeUnit.SECONDS);

        driver.get("http://heatclinic.shiftedtech.com/");

        //driver.findElement(By.linkText("Login")).click();
        driver.findElement(By.partialLinkText("logi")).click();
        driver.navigate().back();
        driver.navigate().forward();
        driver.navigate().refresh();

        driver.findElement(By.name("j_username")).sendKeys("mithucu2000@yahoo.com");
        driver.findElement(By.name("j_password")).sendKeys("123456");
        driver.findElement(By.cssSelector(".login_button.big.red")).click();

        driver.findElement(By.linkText("Register")).click();

        driver.findElement(By.name("email")).sendKeys("mithucu2000@yahoo.com");
        driver.findElement(By.name("customer.firstName")).sendKeys("Mithu");
        driver.findElement(By.name("customer.lastName")).sendKeys("Zaw");
        driver.findElement(By.name("password")).sendKeys("123456");
        driver.findElement(By.name("passwordConfirm")).sendKeys("123456");
        driver.findElement(By.cssSelector(".register_button.big.red")).click();


    }
}
